package com.gradedlab.core;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class PerformerTest {
   Performer performer;
	@Before 
	public void setup() {
		performer = new Performer(1);
	}
	
	@Test
	public void testPerformMethodWithPositiveId() {
		 assertEquals(1,performer.perform());
	}
	
	 @Rule
	   public ExpectedException negativeIdException = ExpectedException.none();
	@Test
	public void testPerformMethodWithNegativeId() {
		negativeIdException.expect(IllegalArgumentException.class);
		negativeIdException.expectMessage("Id cannot be negative");
		new Performer(-1).perform();
	}
	
	@Test
	public void testGetIdMethodWithIdAs1() {
		 assertEquals(1,performer.getId());
	}
	
	@Test
	public void testGetIdMethodWithNegativeId() {
		negativeIdException.expect(IllegalArgumentException.class);
		negativeIdException.expectMessage("Id cannot be negative");
		new Performer(-1).getId();
	}
	
	@Test
	public void testPerformerConstructorWithNegativeId() {
		
		   negativeIdException.expect(IllegalArgumentException.class);
		   negativeIdException.expectMessage("Id cannot be negative");
		   new Performer(-1);
	}

}
