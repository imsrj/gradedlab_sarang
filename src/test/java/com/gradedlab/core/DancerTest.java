package com.gradedlab.core;

import static org.junit.Assert.*;


import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;


public class DancerTest {
	//final static Logger logger = Logger.getLogger(AuditionTest.class);
	  Performer performer;
		@Before 
		public void setup() {
			performer = new Dancer(1, "tap");
		}
		
		@Test
		public void testPerformMethodWithIdAsPositive() {
			 assertEquals(1,performer.perform());
		}
		
		
		 @Rule
		   public ExpectedException negativeIdException = ExpectedException.none();
		 
		 @Test
			public void performMethodWithNegativeId() {
				negativeIdException.expect(IllegalArgumentException.class);
				negativeIdException.expectMessage("Id cannot be negative");
				new Performer(-1).getId();
			}
}
