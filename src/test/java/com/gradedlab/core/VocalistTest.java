package com.gradedlab.core;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class VocalistTest {

	Performer performer1;
	Performer performer2;
	@Before 
	public void setup() {
		performer1 = new Vocalist(1, 'G');
		performer2 = new Vocalist(1, 'A', 10);
		
	}
	
	@Test
	public void testPerformMethodWithoutVolumeWithPositiveId() {
		 assertEquals(1,performer1.perform());
	}
	
	@Test
	public void testPerformMethodWithVolume10WithIdAsPositiveId() {
		 assertEquals(1,performer2.perform());
	}

	
	 @Rule
	   public ExpectedException negativeIdException = ExpectedException.none();
	 
	 @Test
		public void testPerformMethodWithNegativeId() {
			negativeIdException.expect(IllegalArgumentException.class);
			negativeIdException.expectMessage("Id cannot be negative");
			new Vocalist(-1, 'G');
		}
	 
	 @Test
		public void testVocalistConstructorWithInvalidVolume() {
			negativeIdException.expect(IllegalArgumentException.class);
			negativeIdException.expectMessage("Given volume is invalid, ranges from 1-10");
			new Vocalist(1, 'G', 11);
		}
}
