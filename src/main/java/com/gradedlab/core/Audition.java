package com.gradedlab.core;

import java.util.ArrayList;
import java.util.List;

class Audition {
    List<Performer> performerList = new ArrayList<Performer>();
    
    int addPerformer(int id) throws IllegalArgumentException{
    	if(checkPerformerAlreadyExists(id))
    		throw new IllegalArgumentException("Perfomer already exists.");
    	
    	performerList.add(new Performer(id));
    	return 1;
    	
    }
    int addDancer(int id, String style)  throws IllegalArgumentException{
		// TODO Auto-generated method stub
    	if(checkPerformerAlreadyExists(id)) 
    		throw new IllegalArgumentException("Perfomer already exists.");
    	
    	performerList.add(new Dancer(id, style));
		return 1;
	}

     int addVocalist(int id, char key)  throws IllegalArgumentException{
		// TODO Auto-generated method stub
		
		if(checkPerformerAlreadyExists(id))
    		throw new IllegalArgumentException("Perfomer already exists.");
    	
    	performerList.add(new Vocalist(id, key));
		return 1;
	 }
	
	int addVocalistWithVolume(int id, char key, int vol)  throws IllegalArgumentException{
		// TODO Auto-generated method stub
		
		if(checkPerformerAlreadyExists(id))
    		throw new IllegalArgumentException("Perfomer already exists.");
    	
    	performerList.add(new Vocalist(id, key, vol));
		return 1;

	}


	private boolean checkPerformerAlreadyExists(int id) {
		// TODO Auto-generated method stub
		for(int i=0; i<performerList.size(); i++) {
			
			if(performerList.get(i).getId() == id) 
				return true;
		    
		}
		return false;
	}
	
	int startAudition() {
		for(int i=0; i<performerList.size(); i++) {
			performerList.get(i).perform();
		}
		return 1;
	}

}
