package com.gradedlab.core;

class Vocalist extends Performer {
	  
	  private final char key;  
	  private int volume=0;
	  
	  Vocalist(int id, char key) {
		 super(id);
		 this.key = key;
	  }
	  Vocalist(int id, char key, int vol) throws IllegalArgumentException{
			 super(id);
			 this.key = key;
			 
			 if(vol<1 || vol>10)
				 throw new IllegalArgumentException("Given volume is invalid, ranges from 1-10");
			 this.volume = vol;
		  }
    
	  int perform() {
		if(this.volume==0)
	    System.out.println("I sing in the key of-" + this.key + "- " + this.id);
		
		else
			System.out.println("I sing in the key of-" + this.key + "- " + "at the volume " + this.volume + "- "+ this.id);
	    return 1;
	}
	  
	  
}
